package com.rapidtech.restapi.entity;

import com.rapidtech.restapi.model.CustomerModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

import javax.persistence.*;
import java.util.Date;


@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
@Table(name = "customer_tab")
public class CustomerEntity {
    @Id
    @TableGenerator(name = "id_generator", table = "sequence_tab",
            pkColumnName = "gen_name", valueColumnName = "gen_value",
            pkColumnValue="customer_id", initialValue=0, allocationSize=0)
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "id_generator")
    private Long id;

    @Column(name = "customer_name", length = 100, nullable = false)
    private String fullName;

    @Column(name = "customer_address", length = 255, nullable = false)
    private String address;

    @Column(name = "customer_city", length = 255, nullable = false)
    private String city;

    @Column(name = "customer_gender", length = 100, nullable = false)
    private String gender;

    @Column(name = "customer_dateOfBirth", length = 255, nullable = false)
    private Date dateOfBirth;

    @Column(name = "customer_dateOfPlace", length = 255, nullable = false)
    private String dateOfPlace;

    public CustomerEntity(CustomerModel model) {
        BeanUtils.copyProperties(model, this);
    }



}

